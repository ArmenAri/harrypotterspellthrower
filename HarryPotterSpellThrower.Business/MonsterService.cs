﻿using HarryPotterMonsterThrower.DAL;
using System;
using System.Collections.Generic;
using HarryPotterSpellThrower.DTO;
using HarryPotterSpellThrower.Business.Interfaces;

namespace HarryPotterMonsterThrower.Business
{
    public class MonsterService : IMonsterService
    {
        public MonsterDto AddMonster(MonsterDto monster)
        {
            if (monster != null)
            {
                using (MonsterRepository _monsterRepo = new MonsterRepository())
                {
                    return _monsterRepo.AddMonster(monster);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(monster), "Monster can't be null");
            }
        }

        public MonsterDto GetMonster(int Id)
        {
            using (MonsterRepository _monsterRepo = new MonsterRepository())
            {
                return _monsterRepo.GetMonster(Id);
            }
        }

        public IList<MonsterDto> GetMonsters()
        {
            using (MonsterRepository _monsterRepo = new MonsterRepository())
            {
                return _monsterRepo.GetMonsters();
            }
        }
    }
}
