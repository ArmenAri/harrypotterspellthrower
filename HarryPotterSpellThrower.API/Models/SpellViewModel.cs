﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HarryPotterSpellThrower.API.Models
{
    /// <summary>
    /// SpellViewModel class
    /// </summary>
    public class SpellViewModel
    {

        /// <summary>
        /// Id of the spell should not be NULL.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the spell.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description of the spell.
        /// </summary>
        public string Description { get; set; }
    }
}