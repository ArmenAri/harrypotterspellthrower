﻿using HarryPotterSpellThrower.DTO;
using System.Collections.Generic;


namespace HarryPotterSpellThrower.Business.Interfaces
{
    interface ILevelService
    {
        IList<LevelDto> GetLevels();
        LevelDto GetLevel(int Id);
        LevelDto AddLevel(LevelDto level);
    }
}
