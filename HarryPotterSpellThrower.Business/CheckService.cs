﻿using HarryPotterLevelThrower.Business;
using HarryPotterPlayerThrower.DAL;
using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPotterSpellThrower.Business
{
    public class CheckService
    {
        public CheckDto GetEffectiveSpells(int Id, List<int> SpellIds)
        {
            using (PlayerRepository _playerRepo = new PlayerRepository())
            {
                LevelService _levelService = new LevelService();
                PlayerDto player = _playerRepo.GetPlayer(Id);
                List<MonsterDto> monsters = player.Level.Monsters.ToList();
                List<int> monsterSpellsIds = new List<int>();

                foreach (MonsterDto monster in monsters)
                {
                    monsterSpellsIds.AddRange(from SpellDto spell in monster.Spells
                                              select spell.Id);
                }

                monsterSpellsIds.Sort();
                SpellIds.Sort();

                bool match = monsterSpellsIds.SequenceEqual(SpellIds);
                bool win = (match && player.LevelId + 1 > _levelService.GetLevels().Count()) ? true : false;

                if (match && !win)
                    _playerRepo.UpdatePlayer(new PlayerDto
                    {
                        Id = player.Id,
                        Name = player.Name,
                        LevelId = player.LevelId + 1,
                        Level = _levelService.GetLevel(player.LevelId + 1)
                    });

                return new CheckDto {
                    Win = win,
                    Match = match,
                };
            }
        }
    }
}
