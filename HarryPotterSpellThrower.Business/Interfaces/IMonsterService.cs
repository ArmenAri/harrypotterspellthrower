﻿using HarryPotterSpellThrower.DTO;
using System.Collections.Generic;


namespace HarryPotterSpellThrower.Business.Interfaces
{
    interface IMonsterService
    {
        IList<MonsterDto> GetMonsters();
        MonsterDto GetMonster(int Id);
        MonsterDto AddMonster(MonsterDto player);
    }
}
