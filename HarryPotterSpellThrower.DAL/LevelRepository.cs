﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.DAL.Interfaces;
using HarryPotterSpellThrower.DTO;

namespace HarryPotterLevelThrower.DAL
{
    public class LevelRepository : IDisposable, ILevelRepository
    {
        private readonly hpstEntities _dbcontext = null;

        public LevelRepository()
        {
            _dbcontext = new hpstEntities();
        }

        public LevelRepository(hpstEntities context)
        {
            _dbcontext = context;
        }

        public List<LevelDto> GetLevels()
        {
            try
            {
                //Get all level data line from database 
                List<Level> levelsEntities = _dbcontext.Levels.ToList();
                //transform to DTO, and send to upper layer
                return levelsEntities.Select(x => new LevelDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Text = x.Text,
                    Image = x.Image,
                    Monsters = x.LevelMonsters.Select(y => new MonsterDto
                    {
                        Id = y.Monster.Id,
                        Name = y.Monster.Name,
                        Text = y.Monster.Text,
                        Image = y.Monster.Image,
                        Spells = y.Monster.MonsterSpells.Select(z => new SpellDto
                        {
                            Id = z.Spell.Id,
                            Name = z.Spell.Name,
                            Description = z.Spell.Description
                        }).ToList()
                    }).ToList()
                }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public LevelDto GetLevel(int Id)
        {
            try
            {
                //Get all level data line from database 
                List<Level> levelEntities = _dbcontext.Levels.ToList();
                //transform to DTO, and send to upper layer
                Level s = levelEntities.Find(x => x.Id == Id);
                return new LevelDto { 
                    Id = s.Id, 
                    Name = s.Name,
                    Text = s.Text, 
                    Image = s.Image, 
                    Monsters = s.LevelMonsters.Select(x => new MonsterDto
                    {
                        Id = x.Monster.Id,
                        Name = x.Monster.Name,
                        Text = x.Monster.Text,
                        Image = x.Monster.Image,
                        Spells = x.Monster.MonsterSpells.Select(y => new SpellDto
                        {
                            Id = y.Spell.Id,
                            Name = y.Spell.Name,
                            Description = y.Spell.Description
                        }).ToList()
                    }).ToList()
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }

        public LevelDto AddLevel(LevelDto level)
        {
            Level newLevel = level.ToEntity();
            var levelCreated = _dbcontext.Levels.Add(newLevel);
            _dbcontext.SaveChanges();
            return levelCreated.ToDto();
        }
    }
}
