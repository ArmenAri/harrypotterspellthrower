﻿using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPotterSpellThrower.DAL.Interfaces
{
    interface IMonsterRepository
    {
        List<MonsterDto> GetMonsters();
        MonsterDto GetMonster(int Id);
        MonsterDto AddMonster(MonsterDto monster);
    }
}
