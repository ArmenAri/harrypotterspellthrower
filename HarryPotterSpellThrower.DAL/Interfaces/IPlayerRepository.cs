﻿using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPotterSpellThrower.DAL.Interfaces
{
    interface IPlayerRepository
    {
        List<PlayerDto> GetPlayers();
        PlayerDto GetPlayer(int Id);
        PlayerDto AddPlayer(PlayerDto player);
        PlayerDto UpdatePlayer(PlayerDto player);
    }
}
