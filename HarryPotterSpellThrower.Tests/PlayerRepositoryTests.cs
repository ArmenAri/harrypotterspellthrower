﻿using HarryPotterPlayerThrower.DAL;
using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.DTO;
using HarryPotterSpellThrower.Tests.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace HarryPotterPlayerThrower.Tests
{
    [TestClass]
    public class PlayerRepositoryTests
    {
        [TestMethod]
        public void GetPlayers()
        {
            var data = new List<Player>
            {
                new Player { Name = "BBB" },
                new Player { Name = "ZZZ" },
                new Player { Name = "AAA" },
            }.AsQueryable(); 

            var mockSet = new Mock<DbSet<Player>>();
            mockSet.As<IQueryable<Player>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Player>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Player>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Player>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<hpstEntities>();
            mockContext.Setup(c => c.Players).Returns(mockSet.Object);

            var repo = new PlayerRepository(mockContext.Object);
            var spells = repo.GetPlayers();

            Assert.AreEqual(3, spells.Count);
            Assert.AreEqual("BBB", spells[0].Name);
            Assert.AreEqual("ZZZ", spells[1].Name);
            Assert.AreEqual("AAA", spells[2].Name);
        }

        [TestMethod]
        public void AddPlayer()
        {
            var data = new List<Player>
            {
                new Player { Id = 1, Name = "test" },
                new Player { Id = 2, Name = "test" },
                new Player { Id = 3, Name = "test" },
            };
            var mockSet = Tools.GetQueryableMockDbSet<Player>(data);
            var mockContext = new Mock<hpstEntities>();
            mockContext.Setup(m => m.Players).Returns(mockSet.Object);
            var service = new PlayerRepository(mockContext.Object);
            int initCount = data.Count();
            var result = service.AddPlayer(new PlayerDto { Name = "test" });
            int resultCount = data.Count();
            mockSet.Verify(m => m.Add(It.IsAny<Player>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
            Assert.AreEqual(initCount + 1, resultCount);
        }
    }
}
