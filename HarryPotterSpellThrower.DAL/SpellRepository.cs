﻿using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.DAL.Interfaces;
using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HarryPotterSpellThrower.DAL
{
    public class SpellRepository : IDisposable, ISpellRepository
    {
        private readonly hpstEntities _dbcontext = null;

        public SpellRepository()
        {
            _dbcontext = new hpstEntities();
        }

        public SpellRepository(hpstEntities context)
        {
            _dbcontext = context;
        }

        public List<SpellDto> GetSpells()
        {
            try
            {
                //Get all spell data line from database 
                List<Spell> spellsEntities = _dbcontext.Spells.ToList();
                //transform to DTO, and send to upper layer
                return spellsEntities.Select(x => new SpellDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public SpellDto GetSpell(int Id)
        {
            try
            {
                //Get all spell data line from database 
                List<Spell> spellEntities = _dbcontext.Spells.ToList();
                //transform to DTO, and send to upper layer
                Spell s = spellEntities.Find(x => x.Id == Id);
                return new SpellDto { 
                    Id = s.Id, 
                    Name = s.Name, 
                    Description = s.Description 
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }

        public SpellDto AddSpell(SpellDto spell)
        {
            Spell newSpell = spell.ToEntity();
            var spellCreated = _dbcontext.Spells.Add(newSpell);
            _dbcontext.SaveChanges();
            return spellCreated.ToDto();
        }
    }
}
