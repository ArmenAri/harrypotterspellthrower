﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPotterSpellThrower.DTO
{
    public class PlayerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LevelId { get; set; }
        public LevelDto Level { get; set; }
    }
}
