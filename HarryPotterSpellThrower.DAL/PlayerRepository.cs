﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.DAL.Interfaces;
using HarryPotterSpellThrower.DTO;

namespace HarryPotterPlayerThrower.DAL
{
    public class PlayerRepository : IDisposable, IPlayerRepository
    {
        private readonly hpstEntities _dbcontext = null;

        public PlayerRepository()
        {
            _dbcontext = new hpstEntities();
        }

        public PlayerRepository(hpstEntities context)
        {
            _dbcontext = context;
        }

        public List<PlayerDto> GetPlayers()
        {
            try
            {
                //Get all player data line from database 
                List<Player> playersEntities = _dbcontext.Players.ToList();
                //transform to DTO, and send to upper layer
                return playersEntities.Select(x => new PlayerDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    LevelId = x.LevelId,
                    Level = _dbcontext.Levels?.ToList().Find(i => i.Id == x.LevelId).ToDto()
                }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public PlayerDto GetPlayer(int Id)
        {
            try
            {
                //Get all player data line from database 
                List<Player> playerEntities = _dbcontext.Players.ToList();
                //transform to DTO, and send to upper layer
                Player s = playerEntities.Find(x => x.Id == Id);
                return new PlayerDto { Id = s.Id, Name = s.Name, LevelId = s.LevelId, Level = _dbcontext.Levels?.ToList().Find(x => x.Id == s.LevelId).ToDto() };
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }

        public PlayerDto AddPlayer(PlayerDto player)
        {
            Player newPlayer = player.ToEntity();
            var playerCreated = _dbcontext.Players.Add(newPlayer);
            _dbcontext.SaveChanges();
            return playerCreated.ToDto();
        }

        public PlayerDto UpdatePlayer(PlayerDto player)
        {
            var existingPlayer = _dbcontext.Players.Find(player.Id);
            if (existingPlayer != null)
            {
                try
                {
                    existingPlayer.Id = player.Id;
                    existingPlayer.Name = player.Name;
                    existingPlayer.LevelId = player.LevelId;
                    existingPlayer.Level = _dbcontext.Levels?.ToList().Find(x => x.Id == player.LevelId);

                    _dbcontext.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
            return existingPlayer.ToDto();
        }
    }
}
