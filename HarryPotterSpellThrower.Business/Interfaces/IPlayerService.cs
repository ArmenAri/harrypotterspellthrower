﻿using HarryPotterSpellThrower.DTO;
using System.Collections.Generic;


namespace HarryPotterSpellThrower.Business.Interfaces
{
    interface IPlayerService
    {
        IList<PlayerDto> GetPlayers();
        PlayerDto GetPlayer(int Id);
        PlayerDto AddPlayer(PlayerDto player);
        PlayerDto UpdatePlayer(PlayerDto player);
    }
}
