﻿using System.Collections.Generic;

namespace HarryPotterSpellThrower.DTO
{
    public class MonsterSpellDto
    {
        public int Id { get; set; }
        public int MonsterId { get; set; }
        public int SpellId { get; set; }
        public virtual MonsterDto Monster { get; set; }
        public virtual SpellDto Spell { get; set; }
    }
}