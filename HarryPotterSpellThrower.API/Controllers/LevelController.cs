﻿using HarryPotterLevelThrower.Business;
using HarryPotterSpellThrower.API.Models;
using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HarryPotterLevelThrower.API.Controllers
{
    /// <summary>
    /// Level Controller class.
    /// </summary>
    public class LevelController : ApiController
    {

        private readonly LevelService _levelService = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelController"/> class.
        /// </summary>
        public LevelController()
        {
            _levelService = new LevelService();
        }

        /// <summary>
        /// Get list of all Levels.
        /// </summary>
        /// <remarks>
        /// Get list of all Levels from the database no filtered
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"> List of Level </response>
        [ResponseType(typeof(IEnumerable<LevelViewModel>))]
        public IHttpActionResult Get()
        {
            IList<LevelDto> levels = _levelService.GetLevels();
            IEnumerable<LevelViewModel> levelList = levels.Select(s => new LevelViewModel
            {
                Id = s.Id,
                Name = s.Name,
                Text = s.Text,
                Image = s.Image,
                Monsters = s.Monsters
            });

            return Ok(levelList);

        }

        /// <summary>
        /// Get the specified Level.
        /// </summary>
        /// <param name="Id">The Id.</param>
        /// <response code="200"> Level </response>
        /// <response code="404"> Not Found </response>
        public IHttpActionResult Get(int Id)
        {
            IList<LevelDto> levels = _levelService.GetLevels();
            LevelDto level = levels.Where(s => s.Id == Id).First();

            return (level == null) ? NotFound() : (IHttpActionResult)Ok(new LevelViewModel
            {
                Id = level.Id,
                Name = level.Name,
                Text = level.Text,
                Image = level.Image,
                Monsters = level.Monsters
            });
        }

        /// <summary>
        /// Posts the specified Level.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <response code="200"> Created </response>
        /// <response code="400"> Parameter issue </response>
        /// <response code="500"> Other issues, see message included </response>
        public IHttpActionResult Post([FromBody]LevelViewModel level)
        {
            if (level == null)
            {
                return BadRequest("Level Id is required");
            }

            try
            {
                _levelService.AddLevel(new LevelDto
                {
                    Id = level.Id,
                    Name = level.Name,
                    Text = level.Text,
                    Image = level.Image,
                    Monsters = level.Monsters
                });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }
    }
}