﻿using HarryPotterPlayerThrower.DAL;
using System;
using System.Collections.Generic;
using HarryPotterSpellThrower.DTO;
using HarryPotterSpellThrower.Business.Interfaces;
using System.Linq;

namespace HarryPotterPlayerThrower.Business
{
    public class PlayerService : IPlayerService
    {
        public PlayerDto AddPlayer(PlayerDto player)
        {
            if (player != null)
            {
                using (PlayerRepository _playerRepo = new PlayerRepository())
                {
                    return _playerRepo.AddPlayer(player);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(player), "Player can't be null");
            }
        }

        public PlayerDto GetPlayer(int Id)
        {
            using (PlayerRepository _playerRepo = new PlayerRepository())
            {
                return _playerRepo.GetPlayer(Id);
            }
        }

        public IList<PlayerDto> GetPlayers()
        {
            using (PlayerRepository _playerRepo = new PlayerRepository())
            {
                return _playerRepo.GetPlayers();
            }
        }

        public PlayerDto UpdatePlayer(PlayerDto player)
        {
            using (PlayerRepository _playerRepo = new PlayerRepository())
            {
                return _playerRepo.UpdatePlayer(player);
            }
        }
    }
}