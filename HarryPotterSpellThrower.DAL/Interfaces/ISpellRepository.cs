﻿using HarryPotterSpellThrower.DTO;
using System.Collections.Generic;


namespace HarryPotterSpellThrower.DAL.Interfaces
{
    interface ISpellRepository
    {
        List<SpellDto> GetSpells();
        SpellDto GetSpell(int Id);
        SpellDto AddSpell(SpellDto item);
    }
}
