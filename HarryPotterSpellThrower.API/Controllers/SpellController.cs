﻿using HarryPotterSpellThrower.Business;
using HarryPotterSpellThrower.API.Models;
using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HarryPotterSpellThrower.API.Controllers
{
    /// <summary>
    /// Spell Controller class.
    /// </summary>
    public class SpellController : ApiController
    {

        private readonly SpellService _spellService = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpellController"/> class.
        /// </summary>
        public SpellController()
        {
            _spellService = new SpellService();
        }

        /// <summary>
        /// Get list of all Spells.
        /// </summary>
        /// <remarks>
        /// Get list of all Spells from the database no filtered
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"> List of Spell </response>
        [ResponseType(typeof(IEnumerable<SpellViewModel>))]
        public IHttpActionResult Get()
        {
            IList<SpellDto> spells = _spellService.GetSpells();
            IEnumerable<SpellViewModel> spellList = spells.Select(s => new SpellViewModel
            {
                Id = s.Id,
                Name = s.Name,
                Description = s.Description

            });

            return Ok(spellList);

        }

        /// <summary>
        /// Get the specified Spell.
        /// </summary>
        /// <param name="Id">The Id.</param>
        /// <response code="200"> Spell </response>
        /// <response code="404"> Not Found </response>
        public IHttpActionResult Get(int Id)
        {
            IList<SpellDto> spells = _spellService.GetSpells();
            SpellDto spell = spells.Where(s => s.Id == Id).First();

            return (spell == null) ? NotFound() : (IHttpActionResult)Ok(new SpellViewModel
            {
                Id = spell.Id,
                Name = spell.Name,
                Description = spell.Description
            });
        }

        /// <summary>
        /// Posts the specified Spell.
        /// </summary>
        /// <param name="spell">The spell.</param>
        /// <response code="200"> Created </response>
        /// <response code="400"> Parameter issue </response>
        /// <response code="500"> Other issues, see message included </response>
        public IHttpActionResult Post([FromBody]SpellViewModel spell)
        {
            if (spell == null)
            {
                return BadRequest("Spell Id is required");
            }

            try
            {
                _spellService.AddSpell(new SpellDto
                {
                    Id = spell.Id,
                    Name = spell.Name,
                    Description = spell.Description
                });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }
    }
}