﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HarryPotterSpellThrower.DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class hpstEntities : DbContext
    {
        public hpstEntities()
            : base("name=hpstEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<LevelMonster> LevelMonsters { get; set; }
        public virtual DbSet<Level> Levels { get; set; }
        public virtual DbSet<Monster> Monsters { get; set; }
        public virtual DbSet<MonsterSpell> MonsterSpells { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Spell> Spells { get; set; }
    }
}
