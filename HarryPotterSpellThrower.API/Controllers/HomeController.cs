﻿using System.Web.Mvc;

namespace HarryPotterSpellThrower.API.Controllers
{
    /// <summary>
    /// HomeController class
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Index
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }
    }
}