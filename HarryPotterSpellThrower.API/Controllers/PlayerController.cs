﻿using HarryPotterPlayerThrower.Business;
using HarryPotterSpellThrower.API.Models;
using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HarryPotterPlayerThrower.API.Controllers
{
    /// <summary>
    /// Player Controller class.
    /// </summary>
    public class PlayerController : ApiController
    {

        private readonly PlayerService _playerService = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerController"/> class.
        /// </summary>
        public PlayerController()
        {
            _playerService = new PlayerService();
        }

        /// <summary>
        /// Get list of all Players.
        /// </summary>
        /// <remarks>
        /// Get list of all Players from the database no filtered
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"> List of Player </response>
        [ResponseType(typeof(IEnumerable<PlayerViewModel>))]
        public IHttpActionResult Get()
        {
            IList<PlayerDto> players = _playerService.GetPlayers();
            IEnumerable<PlayerViewModel> playerList = players.Select(s => new PlayerViewModel
            {
                Id = s.Id,
                Name = s.Name,
                LevelId = s.LevelId,
                Level = s.Level
            });

            return Ok(playerList);

        }

        /// <summary>
        /// Get the specified Player.
        /// </summary>
        /// <param name="Id">The Id.</param>
        /// <response code="200"> Player </response>
        /// <response code="404"> Not Found </response>
        public IHttpActionResult Get(int Id)
        {
            IList<PlayerDto> players = _playerService.GetPlayers();
            PlayerDto player = players.Where(s => s.Id == Id).First();

            return (player == null) ? NotFound() : (IHttpActionResult)Ok(new PlayerViewModel
            {
                Id = player.Id,
                Name = player.Name,
                LevelId = player.LevelId,
                Level = player.Level
            });
        }

        /// <summary>
        /// Posts the specified Player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <response code="200"> Created </response>
        /// <response code="400"> Parameter issue </response>
        /// <response code="500"> Other issues, see message included </response>
        public IHttpActionResult Post([FromBody]PlayerViewModel player)
        {
            if (player == null)
            {
                return BadRequest("Player Id is required");
            }

            try
            {
                _playerService.AddPlayer(new PlayerDto
                {
                    Id = player.Id,
                    Name = player.Name,
                    LevelId = player.LevelId,
                    Level = player.Level
                });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }

        /// <summary>
        /// Update the specified Player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <response code="200"> Updated </response>
        /// <response code="400"> Parameter issue </response>
        /// <response code="500"> Other issues, see message included </response>
        public IHttpActionResult Put([FromBody]PlayerViewModel player)
        {
            if (player == null)
            {
                return BadRequest("Player Id is required");
            }

            try
            {
                _playerService.UpdatePlayer(new PlayerDto
                {
                    Id = player.Id,
                    Name = player.Name,
                    LevelId = player.LevelId,
                    Level = player.Level
                });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}