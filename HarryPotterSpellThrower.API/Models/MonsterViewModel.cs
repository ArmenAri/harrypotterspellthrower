﻿using HarryPotterSpellThrower.DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HarryPotterSpellThrower.API.Models
{
    /// <summary>
    /// MonsterViewModel class
    /// </summary>
    public class MonsterViewModel
    {
        /// <summary>
        /// Id of the monster should not be NULL.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the monster.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Text that represent the monster and give hint on how to kill him.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Id of the level in which is present this monster.
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Image that represent the monster.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// List of spells that are efficient on this monster.
        /// </summary>
        public virtual ICollection<SpellDto> Spells { get; set; }
    }
}