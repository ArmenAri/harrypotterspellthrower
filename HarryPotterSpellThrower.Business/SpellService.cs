﻿using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.Business.Interfaces;
using System;
using System.Collections.Generic;
using HarryPotterSpellThrower.DTO;


namespace HarryPotterSpellThrower.Business
{
    public class SpellService : ISpellService
    {
        public SpellDto AddSpell(SpellDto spell)
        {
            if (spell != null)
            {
                using (SpellRepository _spellRepo = new SpellRepository())
                {
                    return _spellRepo.AddSpell(spell);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(spell), "Spell can't be null");
            }
        }

        public SpellDto GetSpell(int Id)
        {
            using (SpellRepository _spellRepo = new SpellRepository())
            {
                return _spellRepo.GetSpell(Id);
            }
        }

        public IList<SpellDto> GetSpells()
        {
            using (SpellRepository _spellRepo = new SpellRepository())
            {
                return _spellRepo.GetSpells();
            }
        }
    }
}
