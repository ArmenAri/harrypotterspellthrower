﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPotterSpellThrower.DTO
{
    public class CheckDto
    {
        public bool Win { get; set; }
        public bool Match { get; set; }
    }
}
