﻿using HarryPotterSpellThrower.DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HarryPotterSpellThrower.API.Models
{
    /// <summary>
    /// LevelViewModel class
    /// </summary>
    public class LevelViewModel
    {
        /// <summary>
        /// Id of the level should not be NULL.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the level.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Text that is displayed when players arrives inside.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// URL of the background image of the level.
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// List of monsters that are present inside the level.
        /// </summary>
        public ICollection<MonsterDto> Monsters { get; set; }
    }
}