﻿using HarryPotterLevelThrower.Business;
using HarryPotterPlayerThrower.Business;
using HarryPotterSpellThrower.API.Models;
using HarryPotterSpellThrower.Business;
using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HarryPotterPlayerThrower.API.Controllers
{
    /// <summary>
    /// Player Controller class.
    /// </summary>
    public class CheckController : ApiController
    {

        private readonly CheckService _checkService = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerController"/> class.
        /// </summary>
        public CheckController()
        {
            _checkService = new CheckService();
        }

        /// <summary>
        /// Get the specified Player.
        /// </summary>
        /// <param name="Id">The Id.</param>
        /// <param name="SpellIds"> Spells Ids.</param>
        /// <response code="200"> Ok </response>
        public IHttpActionResult Get(int Id, [FromUri] List<int> SpellIds)
        {
            try
            {
                return Ok(_checkService.GetEffectiveSpells(Id, SpellIds));
            } catch (Exception e)
            {
                return InternalServerError(e);
            }
            
        }
    }
}