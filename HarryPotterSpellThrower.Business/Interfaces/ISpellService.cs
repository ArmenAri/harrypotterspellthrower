﻿using HarryPotterSpellThrower.DTO;
using System.Collections.Generic;


namespace HarryPotterSpellThrower.Business.Interfaces
{
    interface ISpellService
    {
        IList<SpellDto> GetSpells();
        SpellDto GetSpell(int Id);
        SpellDto AddSpell(SpellDto spell);
    }
}
