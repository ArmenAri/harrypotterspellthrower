﻿using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HarryPotterSpellThrower.DAL
{
    public static class Extensions
    {
        public static Spell ToEntity(this SpellDto dto) => new Spell
        {
            Name = dto.Name,
            Id = dto.Id,
            Description = dto.Description
        };

        public static SpellDto ToDto(this Spell entity) => entity != null
                ? new SpellDto
                {
                    Name = entity.Name,
                    Id = entity.Id,
                    Description = entity.Description
                }
                : null;
        public static Player ToEntity(this PlayerDto dto) => new Player
        {
            Name = dto.Name,
            Id = dto.Id,
            LevelId = dto.LevelId
        };

        public static PlayerDto ToDto(this Player entity) => entity != null
                ? new PlayerDto
                {
                    Name = entity.Name,
                    Id = entity.Id,
                    LevelId = (int) entity.LevelId
                }
                : null;
        public static Level ToEntity(this LevelDto dto) => new Level
        {
            Name = dto.Name,
            Id = dto.Id,
            Text = dto.Text,
            Image = dto.Image
        };

        public static LevelDto ToDto(this Level entity) => entity != null
                ? new LevelDto
                {
                    Name = entity.Name,
                    Id = entity.Id,
                    Text = entity.Text,
                    Image = entity.Image,
                    Monsters = entity.LevelMonsters.Select(monster => new MonsterDto
                    {
                        Id = monster.Monster.Id,
                        Name = monster.Monster.Name,
                        Text = monster.Monster.Text,
                        Image = monster.Monster.Image,
                        Spells = monster.Monster.MonsterSpells.Select(y => new SpellDto
                        {
                            Id = y.Spell.Id,
                            Name = y.Spell.Name,
                            Description = y.Spell.Description
                        }).ToList()
                    }).ToList()
                }
                : null;

        public static Monster ToEntity(this MonsterDto dto) => new Monster
        {
            Name = dto.Name,
            Id = dto.Id,
            Text = dto.Text,
            Image = dto.Image
        };

        public static MonsterDto ToDto(this Monster entity) => entity != null
                ? new MonsterDto
                {
                    Name = entity.Name,
                    Id = entity.Id,
                    Text = entity.Text,
                    Image = entity.Image,
                    Spells = entity.MonsterSpells.Select(spell => new SpellDto
                    {
                        Id = spell.Spell.Id,
                        Name = spell.Spell.Name,
                        Description = spell.Spell.Description
                    }).ToList()
                }
                : null;

        public static MonsterSpell ToEntity(this MonsterSpellDto dto) => new MonsterSpell
        {
            Id = dto.Id,
            MonsterId = dto.MonsterId,
            SpellId = dto.SpellId,
        };

        public static MonsterSpellDto ToDto(this MonsterSpell entity) => entity != null
                ? new MonsterSpellDto
                {
                    Id = entity.Id,
                    MonsterId = entity.MonsterId,
                    SpellId = entity.SpellId,
                }
                : null;

        public static LevelMonster ToEntity(this LevelMonsterDto dto) => new LevelMonster
        {
            Id = dto.Id,
            MonsterId = dto.MonsterId,
            LevelId = dto.LevelId,
        };

        public static LevelMonsterDto ToDto(this LevelMonster entity) => entity != null
                ? new LevelMonsterDto
                {
                    Id = entity.Id,
                    MonsterId = entity.MonsterId,
                    LevelId = entity.LevelId,
                }
                : null;

        public static ICollection<MonsterDto> ToDto(this ICollection<Monster> entities) => (from Monster monster in entities
                                                                                            select monster.ToDto()).ToList();
        public static ICollection<SpellDto> ToDto(this ICollection<Spell> entities) => (from Spell spell in entities
                                                                                            select spell.ToDto()).ToList();
        public static ICollection<PlayerDto> ToDto(this ICollection<Player> entities) => (from Player player in entities
                                                                                        select player.ToDto()).ToList();
        public static ICollection<Monster> ToEntity(this ICollection<MonsterDto> dtos) => (from MonsterDto monster in dtos
                                                                                           select monster.ToEntity()).ToList();
        public static ICollection<Player> ToEntity(this ICollection<PlayerDto> dtos) => (from PlayerDto player in dtos
                                                                                           select player.ToEntity()).ToList();
        public static ICollection<Spell> ToEntity(this ICollection<SpellDto> dtos) => (from SpellDto spell in dtos
                                                                                           select spell.ToEntity()).ToList();
        public static ICollection<MonsterSpellDto> ToDto(this ICollection<MonsterSpell> entities) => (from MonsterSpell spell in entities
                                                                                            select spell.ToDto()).ToList();
        public static ICollection<LevelMonsterDto> ToDto(this ICollection<LevelMonster> entities) => (from LevelMonster monster in entities
                                                                                                      select monster.ToDto()).ToList();
        public static ICollection<MonsterSpell> ToEntity(this ICollection<MonsterSpellDto> dtos) => (from MonsterSpellDto spell in dtos
                                                                                         select spell.ToEntity()).ToList();
        public static ICollection<LevelMonster> ToEntity(this ICollection<LevelMonsterDto> dtos) => (from LevelMonsterDto monster in dtos
                                                                                                     select monster.ToEntity()).ToList();
    }
}