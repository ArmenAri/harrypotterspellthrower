﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HarryPotterSpellThrower.API.Models
{
    public class CheckViewModel
    {
        /// <summary>
        /// Win true if Player win else false.
        /// </summary>
        public bool Win { get; set; }

        /// <summary>
        /// Match player selected spells with monster spells.
        /// </summary>
        public bool Match { get; set; }
    }
}