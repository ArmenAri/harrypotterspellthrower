﻿using System.Collections.Generic;

namespace HarryPotterSpellThrower.DTO
{
    public class LevelMonsterDto
    {
        public int Id { get; set; }
        public int MonsterId { get; set; }
        public int LevelId { get; set; }
        public virtual LevelDto Level { get; set; }
        public virtual MonsterDto Monster { get; set; }
    }
}