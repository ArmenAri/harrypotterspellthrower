﻿using HarryPotterMonsterThrower.Business;
using HarryPotterSpellThrower.API.Models;
using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace HarryPotterMonsterThrower.API.Controllers
{
    /// <summary>
    /// Monster Controller class.
    /// </summary>
    public class MonsterController : ApiController
    {

        private readonly MonsterService _monsterService = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="MonsterController"/> class.
        /// </summary>
        public MonsterController()
        {
            _monsterService = new MonsterService();
        }

        /// <summary>
        /// Get list of all Monsters.
        /// </summary>
        /// <remarks>
        /// Get list of all Monsters from the database no filtered
        /// </remarks>
        /// <returns></returns>
        /// <response code="200"> List of Monster </response>
        [ResponseType(typeof(IEnumerable<MonsterViewModel>))]
        public IHttpActionResult Get()
        {
            IList<MonsterDto> monsters = _monsterService.GetMonsters();
            IEnumerable<MonsterViewModel> monsterList = monsters.Select(s => new MonsterViewModel
            {
                Id = s.Id,
                Name = s.Name,
                Text = s.Text,
                Image = s.Image,
                Spells = s.Spells
            });

            return Ok(monsterList);

        }

        /// <summary>
        /// Get the specified Monster.
        /// </summary>
        /// <param name="Id">The Id.</param>
        /// <response code="200"> Monster </response>
        /// <response code="404"> Not Found </response>
        public IHttpActionResult Get(int Id)
        {
            IList<MonsterDto> monsters = _monsterService.GetMonsters();
            MonsterDto monster = monsters.Where(s => s.Id == Id).First();

            return (monster == null) ? NotFound() : (IHttpActionResult)Ok(new MonsterViewModel
            {
                Id = monster.Id,
                Name = monster.Name,
                Text = monster.Text,
                Image = monster.Image,
                Spells = monster.Spells
            });
        }

        /// <summary>
        /// Posts the specified Monster.
        /// </summary>
        /// <param name="monster">The monster.</param>
        /// <response code="200"> Created </response>
        /// <response code="400"> Parameter issue </response>
        /// <response code="500"> Other issues, see message included </response>
        public IHttpActionResult Post([FromBody]MonsterViewModel monster)
        {
            if (monster == null)
            {
                return BadRequest("Monster Id is required");
            }

            try
            {
                _monsterService.AddMonster(new MonsterDto
                {
                    Id = monster.Id,
                    Name = monster.Name,
                    Text = monster.Text,
                    Image = monster.Image,
                    Spells = monster.Spells
                });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }
    }
}