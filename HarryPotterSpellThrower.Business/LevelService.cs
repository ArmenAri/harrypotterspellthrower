﻿using HarryPotterLevelThrower.DAL;
using System;
using System.Collections.Generic;
using HarryPotterSpellThrower.DTO;
using HarryPotterSpellThrower.Business.Interfaces;

namespace HarryPotterLevelThrower.Business
{
    public class LevelService : ILevelService
    {
        public LevelDto AddLevel(LevelDto level)
        {
            if (level != null)
            {
                using (LevelRepository _levelRepo = new LevelRepository())
                {
                    return _levelRepo.AddLevel(level);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(level), "Level can't be null");
            }
        }

        public LevelDto GetLevel(int Id)
        {
            using (LevelRepository _levelRepo = new LevelRepository())
            {
                return _levelRepo.GetLevel(Id);
            }
        }

        public IList<LevelDto> GetLevels()
        {
            using (LevelRepository _levelRepo = new LevelRepository())
            {
                return _levelRepo.GetLevels();
            }
        }
    }
}
