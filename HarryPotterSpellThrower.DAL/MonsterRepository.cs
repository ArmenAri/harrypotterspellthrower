﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.DAL.Interfaces;
using HarryPotterSpellThrower.DTO;

namespace HarryPotterMonsterThrower.DAL
{
    public class MonsterRepository : IDisposable, IMonsterRepository
    {
        private readonly hpstEntities _dbcontext = null;

        public MonsterRepository()
        {
            _dbcontext = new hpstEntities();
        }

        public MonsterRepository(hpstEntities context)
        {
            _dbcontext = context;
        }

        public List<MonsterDto> GetMonsters()
        {
            try
            {
                //Get all monster data line from database 
                List<Monster> monstersEntities = _dbcontext.Monsters.ToList();
                //transform to DTO, and send to upper layer
                return monstersEntities.Select(x => new MonsterDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Text = x.Text,
                    Image = x.Image,
                    Spells = x.MonsterSpells.Select(s => new SpellDto
                    {
                        Id = s.Spell.Id,
                        Name = s.Spell.Name,
                        Description = s.Spell.Description
                    }).ToList()
                }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public MonsterDto GetMonster(int Id)
        {
            try
            {
                //Get all monster data line from database 
                List<Monster> monsterEntities = _dbcontext.Monsters.ToList();
                //transform to DTO, and send to upper layer
                Monster s = monsterEntities.Find(x => x.Id == Id);
                return new MonsterDto {
                    Id = s.Id,
                    Name = s.Name,
                    Text = s.Text,
                    Image = s.Image,
                    Spells = s.MonsterSpells.Select(x => new SpellDto
                    {
                        Id = x.Spell.Id,
                        Name = x.Spell.Name,
                        Description = x.Spell.Description
                    }).ToList()
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }

        public MonsterDto AddMonster(MonsterDto monster)
        {
            Monster newMonster = monster.ToEntity();
            var monsterCreated = _dbcontext.Monsters.Add(newMonster);
            _dbcontext.SaveChanges();
            return monsterCreated.ToDto();
        }
    }
}
