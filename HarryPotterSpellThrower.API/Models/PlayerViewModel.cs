﻿using HarryPotterSpellThrower.DTO;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HarryPotterSpellThrower.API.Models
{
    /// <summary>
    /// PlayerViewModel class
    /// </summary>
    public class PlayerViewModel
    {
        /// <summary>
        /// Id of the player should not be NULL.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the player.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Id of the level inside the which the player is.
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Level of the player.
        /// </summary>
        public LevelDto Level { get; set; }
    }
}