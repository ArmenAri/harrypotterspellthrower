﻿using HarryPotterMonsterThrower.DAL;
using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.DTO;
using HarryPotterSpellThrower.Tests.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace HarryPotterMonsterThrower.Tests
{
    [TestClass]
    public class MonsterRepositoryTests
    {

        [TestMethod]
        public void GetMonsters()
        {
            var data = new List<Monster>
            {
                new Monster { Name = "BBB" },
                new Monster { Name = "ZZZ" },
                new Monster { Name = "AAA" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Monster>>();
            mockSet.As<IQueryable<Monster>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Monster>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Monster>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Monster>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<hpstEntities>();
            mockContext.Setup(c => c.Monsters).Returns(mockSet.Object);

            var repo = new MonsterRepository(mockContext.Object);
            var spells = repo.GetMonsters();

            Assert.AreEqual(3, spells.Count);
            Assert.AreEqual("BBB", spells[0].Name);
            Assert.AreEqual("ZZZ", spells[1].Name);
            Assert.AreEqual("AAA", spells[2].Name);
        }

        [TestMethod]
        public void AddMonster()
        {
            var data = new List<Monster>
            {
                new Monster { Id = 1, Name = "test", Text = "Text"},
                new Monster { Id = 2, Name = "test", Text = "Text"},
                new Monster { Id = 3, Name = "test", Text = "Text" },
            };
            var mockSet = Tools.GetQueryableMockDbSet<Monster>(data);
            var mockContext = new Mock<hpstEntities>();
            mockContext.Setup(m => m.Monsters).Returns(mockSet.Object);
            var service = new MonsterRepository(mockContext.Object);
            int initCount = data.Count();
            var result = service.AddMonster(new MonsterDto { Id = 1, Name = "test", Text = "Text", Spells = new List<SpellDto> { } });
            int resultCount = data.Count();
            mockSet.Verify(m => m.Add(It.IsAny<Monster>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
            Assert.AreEqual(initCount + 1, resultCount);
        }
    }
}
