﻿using System.Collections.Generic;

namespace HarryPotterSpellThrower.DTO
{
    public class MonsterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public ICollection<SpellDto> Spells { get; set; }
    }
}