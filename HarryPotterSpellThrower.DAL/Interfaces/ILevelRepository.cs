﻿using HarryPotterSpellThrower.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPotterSpellThrower.DAL.Interfaces
{
    interface ILevelRepository
    {
        List<LevelDto> GetLevels();
        LevelDto GetLevel(int Id);
        LevelDto AddLevel(LevelDto level);
    }
}
