﻿using HarryPotterLevelThrower.DAL;
using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.DTO;
using HarryPotterSpellThrower.Tests.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace HarryPotterLevelThrower.Tests
{
    [TestClass]
    public class LevelRepositoryTests
    {

        [TestMethod]
        public void GetLevels()
        {
            var data = new List<Level>
            {
                new Level { Name = "BBB" },
                new Level { Name = "ZZZ" },
                new Level { Name = "AAA" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Level>>();
            mockSet.As<IQueryable<Level>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Level>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Level>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Level>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<hpstEntities>();
            mockContext.Setup(c => c.Levels).Returns(mockSet.Object);

            var repo = new LevelRepository(mockContext.Object);
            var spells = repo.GetLevels();

            Assert.AreEqual(3, spells.Count);
            Assert.AreEqual("BBB", spells[0].Name);
            Assert.AreEqual("ZZZ", spells[1].Name);
            Assert.AreEqual("AAA", spells[2].Name);
        }

        [TestMethod]
        public void AddLevel()
        {
            var data = new List<Level>
            {
                new Level { Id = 1, Name = "test" },
                new Level { Id = 2, Name = "test" },
                new Level { Id = 3, Name = "test" },
            };
            var mockSet = Tools.GetQueryableMockDbSet<Level>(data);
            var mockContext = new Mock<hpstEntities>();
            mockContext.Setup(m => m.Levels).Returns(mockSet.Object);
            var service = new LevelRepository(mockContext.Object);
            int initCount = data.Count();
            var result = service.AddLevel(new LevelDto { Name = "test", Monsters = new List<MonsterDto> { } });
            int resultCount = data.Count();
            mockSet.Verify(m => m.Add(It.IsAny<Level>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
            Assert.AreEqual(initCount + 1, resultCount);
        }
    }
}
