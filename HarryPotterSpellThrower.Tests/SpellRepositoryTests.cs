﻿using HarryPotterSpellThrower.DAL;
using HarryPotterSpellThrower.DTO;
using HarryPotterSpellThrower.Tests.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace HarryPotterSpellThrower.Tests
{
    [TestClass]
    public class SpellRepositoryTests
    {

        [TestMethod]
        public void GetSpells()
        {
            var data = new List<Spell>
            {
                new Spell { Name = "BBB" },
                new Spell { Name = "ZZZ" },
                new Spell { Name = "AAA" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Spell>>();
            mockSet.As<IQueryable<Spell>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Spell>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Spell>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Spell>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<hpstEntities>();
            mockContext.Setup(c => c.Spells).Returns(mockSet.Object);

            var repo = new SpellRepository(mockContext.Object);
            var spells = repo.GetSpells();

            Assert.AreEqual(3, spells.Count);
            Assert.AreEqual("BBB", spells[0].Name);
            Assert.AreEqual("ZZZ", spells[1].Name);
            Assert.AreEqual("AAA", spells[2].Name);
        }

        [TestMethod]
        public void AddSpell()
        {
            var data = new List<Spell>
            {
                new Spell { Id = 1, Name = "test" },
                new Spell { Id = 2, Name = "test" },
                new Spell { Id = 3, Name = "test" },
            };
            var mockSet = Tools.GetQueryableMockDbSet<Spell>(data);
            var mockContext = new Mock<hpstEntities>();
            mockContext.Setup(m => m.Spells).Returns(mockSet.Object);
            var service = new SpellRepository(mockContext.Object);
            int initCount = data.Count();
            var result = service.AddSpell(new SpellDto { Name = "test" });
            int resultCount = data.Count();
            mockSet.Verify(m => m.Add(It.IsAny<Spell>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
            Assert.AreEqual(initCount + 1, resultCount);
        }
    }
}
